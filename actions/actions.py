# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from cgitb import text
from typing import Any, Text, Dict, List, Union

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, AllSlotsReset
from rasa_sdk.forms import FormValidationAction


import datetime

import requests
import webbrowser, os

class setDomainName(Action):

    def name(self):
        return "set_domain_name"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        intent = tracker.latest_message['intent'].get('name')
        print("class setDomainName: intent > ", intent)
        # accid > accident
        if intent == 'tell_accident_problem_HARD' or intent == 'tell_accident_problem_HARD_TH':
            print("Set domain name:  accid (report accident)")
            return [ SlotSet('domain_name', 'accid')] 
        # lostsm > lost something (lost item)
        if intent == 'lost_stuff' or intent == 'lost_stuff_TH':
            print("Set domain name:  lostm (report lost item)") 
            return [ SlotSet('domain_name', 'lostm')] 
        # lostps > lost person (missing person)
        if intent == 'inform_missing_person' or intent == 'inform_missing_person_TH':
            print("Set domain name:  lostp (report missing person)") 
            return [ SlotSet('domain_name', 'lostp')] 
        # pubutil > public utility
        if intent == 'complain_public_utility' or intent == 'complain_public_utility_TH':
            print("Set domain name:  pubut (complain security > public utility)") 
            return [ SlotSet('domain_name', 'pubut')] 
        # troubmak > trouble maker 
        if intent == 'complain_trouble_maker' or intent == 'complain_trouble_maker_TH':
            print("Set domain name:  troub (complain security > trouble maker)") 
            return [ SlotSet('domain_name', 'troub')] 
        # restau > restaurant
        if intent == 'complain_restaurant' or intent == 'complain_restaurant_TH':
            print("Set domain name:  resta (complain restaurant)") 
            return [ SlotSet('domain_name', 'resta')] 
        # guide > guide/travel agent
        if intent == 'complain_guide' or intent == 'complain_guide_TH':
            print("Set domain name:  guide (complain guide/travel agent)") 
            return [ SlotSet('domain_name', 'guide')] 
        # rentc > car rental shop 
        if intent == 'complain_car_rental_shop' or intent == 'complain_car_rental_shop_TH':
            print("Set domain name:  rentc (complain car rental shop)") 
            return [ SlotSet('domain_name', 'rentc')] 
        # accom > accommodation
        if intent == 'complain_accommodation' or intent == 'complain_accommodation_TH':
            print("Set domain name:  accom (complain accommodation)") 
            return [ SlotSet('domain_name', 'accom')] 
        # touratt > tourist attractions
        if intent == 'complain_tourist_attraction' or intent == 'complain_tourist_attraction_TH':
            print("Set domain name:  tourp (complain tourist attractions)") 
            return [ SlotSet('domain_name', 'tourp')] 
        # othplc > other place
        if intent == 'complain_other_service_place' or intent == 'complain_other_service_place_TH':
            print("Set domain name:  othep (complain other place)") 
            return [ SlotSet('domain_name', 'othep')] 
        # etc > (other complain)
        if intent == 'complain_other' or intent == 'complain_other_TH':
            print("Set domain name:  other (other complain)") 
            return [ SlotSet('domain_name', 'other')] 
       

class setSlotPhoneNum(Action):

    def name(self):
        return "set_slot_val_phone_number"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        intent = tracker.latest_message['intent'].get('name')
        print("class setSlotPhoneNum: intent > ", intent)
        
        if intent == 'no_th_phone_number':
            print("This user doesn't have a Thai phone number")
            phone_ent = tracker.latest_message['entities']
		
            print("Phone num entity: ", phone_ent)
            phone_number = ""

            for e in phone_ent:
                if e['entity'] == "phone_number":
                    phone_number = e['value']
                    
            print("phone number str: ", phone_number)
            
            phone_number = "No phone number"
                
            return [ SlotSet("phone_number", phone_number)] 
        else:   
            return []

class setSlotImageLink(Action):

    def name(self):
        return "set_slot_val_image_link"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        intent = tracker.latest_message['intent'].get('name')
        print("class setSlotImageLink: intent > ", intent)
        
        if intent == 'no_image':
            print("This user doesn't have a photo")
            image_ent = tracker.latest_message['entities']
		
            print("image link entity: ", image_ent)
            image_link = ""

            for e in image_ent:
                if e['entity'] == "image_link":
                    image_link = e['value']
                    
            print("image link str: ", image_link)
            
            image_link = "No photos"
                
            return [ SlotSet("image_link", image_link)]
        if intent == 'no_image_TH':
            print("ผู้ใช้ไม่มีรูปภาพสำหรับแนบประกอบ")
            image_ent = tracker.latest_message['entities']
		
            print("image link entity TH: ", image_ent)
            image_link = ""

            for e in image_ent:
                if e['entity'] == "image_link":
                    image_link = e['value']
                    
            print("image link str TH: ", image_link)
            
            image_link = "ไม่มีรูปถ่าย"
                
            return [ SlotSet("image_link", image_link)]    
        else:   
            return []            

class setSlotEmailAddress(Action):

    def name(self):
        return "set_slot_val_email_address"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        intent = tracker.latest_message['intent'].get('name')
        print("class setSlotEmailAddress: intent > ", intent)
        
        if intent == 'no_email_address':
            print("This user doesn't have an email")
            email_ent = tracker.latest_message['entities']
		
            print("email address entity: ", email_ent)
            email_address = ""

            for e in email_ent:
                if e['entity'] == "email_address":
                    email_address = e['value']
                    
            print("email address str: ", email_address)
            
            email_address = "No email"
                
            return [ SlotSet("email_address", email_address)]
        if intent == 'no_email_address_TH':
            print("ผู้ใช้ไม่มีที่อยู่อีเมล์")
            email_ent = tracker.latest_message['entities']
		
            print("email address entity TH: ", email_ent)
            email_address = ""

            for e in email_ent:
                if e['entity'] == "email_address":
                    email_address = e['value']
                    
            print("email address str TH: ", email_address)
            
            email_address = "ไม่มีอีเมล์"
                
            return [ SlotSet("email_address", email_address)]    
        else:   
            return []           

class SummaryValForm(Action):

    def name(self):
        return "summary_value_in_form"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]: 

        latestAction = tracker.latest_action_name
        print("class SummaryValForm: latestAction > ", latestAction)

        domainName = tracker.get_slot("domain_name")
        report_date = datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        userId = tracker.current_state()["sender_id"]
        # userId = "0006665555"
        print("userId: ", userId)

        if userId.startswith("U"):
            provider_type = "line"    
        else:
            provider_type = "facebook"

        report_date = datetime.datetime.now()
        headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxOTYwMDk5OTk3LCJpYXQiOjE2NDQ3Mzk5OTcsImp0aSI6IjEzYTUwYWFmNWFkNTQ2ODliODVhYzYxMmQ5YmQ2MjY0IiwiYWNjb3VudF90eXBlIjoiYXBpIiwidXNlcl9pZCI6ImNoYXRib3QifQ.L9YineS4C_G0bbGhb2cTnj77cxYqMWVF3OV-YwW3PAU"}

        #report accident
        if domainName == 'accid':
            placeName = tracker.get_slot("place_name")
            situationDetail = tracker.get_slot("situation_detail")
            userName = tracker.get_slot("user_name")
            phoneNumber = tracker.get_slot("phone_number")
            emailAddress = tracker.get_slot("email_address")

            owner_ref = {
            "ref": userId,
            "provider": provider_type,
            "displayName": userName
            }
            print(owner_ref)

            owner_ref_url = 'https://avc.bonmek.com/api/chat/retrieve_ref/'
            post_owner_ref = requests.post(url = owner_ref_url, data = owner_ref, headers=headers)
            print(post_owner_ref.json())

            owner_ref_data = post_owner_ref.json()
            print("owner_ref_data: ", owner_ref_data)
            create_ticket ={
                "title": "Report", 
                "content": "Situation details: " + situationDetail,
                "location": placeName, 
                "category": domainName, 
                "is_urgent": True, 
                "is_incognito": True, 
                "unit_lv1": 1, 
                "telephone": phoneNumber, 
                "email": emailAddress, 
                "owner_ref": owner_ref_data['ref'], 
                "submitted_date_time": report_date
            }
            create_ticket_url = 'https://avc.bonmek.com/api/chat/ticket/'
            ticket = requests.post(url = create_ticket_url, data = create_ticket, headers=headers)
            print(ticket.json())

        #lost item
        elif domainName == 'lostm':
            itemName = tracker.get_slot("item_name")
            placeName = tracker.get_slot("place_name")
            stuffDetail = tracker.get_slot("stuff_detail")
            imageLink = tracker.get_slot("image_link")
            userName = tracker.get_slot("user_name")
            phoneNumber = tracker.get_slot("phone_number")
            emailAddress = tracker.get_slot("email_address")

            # print("itemName: ", type(itemName), "\n val: ", itemName)
            if itemName == None:
                itemName = " "

            fullStuffDetail = itemName + " " + stuffDetail
            # print("fullStuffDetail: ", fullStuffDetail)

            owner_ref = {
            "ref": userId,
            "provider": provider_type,
            "displayName": userName
            }
            print(owner_ref)

            owner_ref_url = 'https://avc.bonmek.com/api/chat/retrieve_ref/'
            post_owner_ref = requests.post(url = owner_ref_url, data = owner_ref, headers=headers)
            print("post_owner_ref  : ", post_owner_ref)
            owner_ref_data = post_owner_ref.json()
            print("owner_ref_data: ", owner_ref_data)

            create_ticket ={
                "title": "Report", 
                "content": "Item details: " + fullStuffDetail + "\n Image link: " + imageLink,
                "location": placeName, 
                "category": domainName, 
                "is_urgent": True, 
                "is_incognito": True, 
                "unit_lv1": 1, 
                "telephone": phoneNumber, 
                "email": emailAddress, 
                "owner_ref": owner_ref_data['ref'], 
                "submitted_date_time": report_date
            }
            create_ticket_url = 'https://avc.bonmek.com/api/chat/ticket/'
            ticket = requests.post(url = create_ticket_url, data = create_ticket, headers=headers)
            print(ticket.json())
        
        #report missing person   
        elif domainName == 'lostp':
            missingPersonName = tracker.get_slot("missing_person_name")
            imageLink = tracker.get_slot("image_link")
            lastTime = tracker.get_slot("lasttime")
            placeName = tracker.get_slot("place_name")
            userName = tracker.get_slot("user_name")
            phoneNumber = tracker.get_slot("phone_number")
            emailAddress = tracker.get_slot("email_address")

            owner_ref = {
            "ref": userId,
            "provider": provider_type,
            "displayName": userName
            }
            owner_ref_url = 'https://avc.bonmek.com/api/chat/retrieve_ref/'
            post_owner_ref = requests.post(url = owner_ref_url, data = owner_ref, headers=headers)

            print(post_owner_ref.json())

            owner_ref_data = post_owner_ref.json()

            create_ticket ={
                "title": "Report", 
                "content": "Missing person name: "+ missingPersonName+"\n Last seen time: "+lastTime+ "\n Image link: " +imageLink,
                "location": placeName, 
                "category": domainName, 
                "is_urgent": True, 
                "is_incognito": True, 
                "unit_lv1": 1, 
                "telephone": phoneNumber, 
                "email": emailAddress, 
                "owner_ref": owner_ref_data['ref'], 
                "submitted_date_time": report_date
            }
            create_ticket_url = 'https://avc.bonmek.com/api/chat/ticket/'
            ticket = requests.post(url = create_ticket_url, data = create_ticket, headers=headers)
            print(ticket.json())
        
        #complain public utilities 
        elif domainName == 'pubut': 
            detailPubutil = tracker.get_slot("detail_of_public_utility")
            placeName = tracker.get_slot("place_name")
            imageLink = tracker.get_slot("image_link")
            userName = tracker.get_slot("user_name")
            phoneNumber = tracker.get_slot("phone_number")
            emailAddress = tracker.get_slot("email_address")

            owner_ref = {
            "ref": userId,
            "provider": provider_type,
            "displayName": userName
            }
            owner_ref_url = 'https://avc.bonmek.com/api/chat/retrieve_ref/'
            post_owner_ref = requests.post(url = owner_ref_url, data = owner_ref, headers=headers)

            print(post_owner_ref.json())

            owner_ref_data = post_owner_ref.json()

            create_ticket ={
                "title": "Complain", 
                "content": "Problem details: "+ detailPubutil + "\n Image link: " +imageLink,
                "location": placeName, 
                "category": domainName, 
                "is_urgent": True, 
                "is_incognito": True, 
                "unit_lv1": 1, 
                "telephone": phoneNumber, 
                "email": emailAddress, 
                "owner_ref": owner_ref_data['ref'], 
                "submitted_date_time": report_date
            }
            create_ticket_url = 'https://avc.bonmek.com/api/chat/ticket/'
            ticket = requests.post(url = create_ticket_url, data = create_ticket, headers=headers)
            print(ticket.json())
            
        #complain trouble maker   
        elif domainName == 'troub':
            detailTroubmak = tracker.get_slot("detail_of_trouble_maker")
            placeName = tracker.get_slot("place_name")
            imageLink = tracker.get_slot("image_link")
            userName = tracker.get_slot("user_name")
            phoneNumber = tracker.get_slot("phone_number")
            emailAddress = tracker.get_slot("email_address")

            owner_ref = {
            "ref": userId,
            "provider": provider_type,
            "displayName": userName
            }
            
            owner_ref_url = 'https://avc.bonmek.com/api/chat/retrieve_ref/'
            post_owner_ref = requests.post(url = owner_ref_url, data = owner_ref, headers=headers)

            print(post_owner_ref.json())

            owner_ref_data = post_owner_ref.json()

            create_ticket ={
                "title": "Complain", 
                "content": "Problem details: "+ detailTroubmak + "\n Image link: " +imageLink,
                "location": placeName, 
                "category": domainName, 
                "is_urgent": True, 
                "is_incognito": True, 
                "unit_lv1": 1, 
                "telephone": phoneNumber, 
                "email": emailAddress, 
                "owner_ref": owner_ref_data['ref'], 
                "submitted_date_time": report_date
            }
            create_ticket_url = 'https://avc.bonmek.com/api/chat/ticket/'
            ticket = requests.post(url = create_ticket_url, data = create_ticket, headers=headers)
            print(ticket.json())

        #complain restaurant / guide / car rental shop / accommodation / tourist attraction / other place
        elif domainName == 'resta' or domainName == 'guide' or domainName == 'rentc' or domainName == 'accom' or domainName == 'tourp' or domainName == 'othep':
            shopName = tracker.get_slot("shop_name")
            placeName = tracker.get_slot("place_name")
            topicComplain = tracker.get_slot("topic_complain")
            complainShopDetail = tracker.get_slot("complain_shop_detail")
            imageLink = tracker.get_slot("image_link")
            userName = tracker.get_slot("user_name")
            phoneNumber = tracker.get_slot("phone_number")
            emailAddress = tracker.get_slot("email_address")

            owner_ref = {
            "ref": userId,
            "provider": provider_type,
            "displayName": userName
            }
            owner_ref_url = 'https://avc.bonmek.com/api/chat/retrieve_ref/'
            post_owner_ref = requests.post(url = owner_ref_url, data = owner_ref, headers=headers)

            print(post_owner_ref.json())

            owner_ref_data = post_owner_ref.json()

            create_ticket ={
                "title": "Complain", 
                "content": "Name of the service: " + shopName+ " Complain about: "+topicComplain+" Details: "+ complainShopDetail + "\n Image link: " +imageLink,
                "location": placeName, 
                "category": domainName, 
                "is_urgent": True, 
                "is_incognito": True, 
                "unit_lv1": 1, 
                "telephone": phoneNumber, 
                "email": emailAddress, 
                "owner_ref": owner_ref_data['ref'], 
                "submitted_date_time": report_date
            }
            create_ticket_url = 'https://avc.bonmek.com/api/chat/ticket/'
            ticket = requests.post(url = create_ticket_url, data = create_ticket, headers=headers)
            print(ticket.json())

        #complain other topic
        elif domainName == 'other':
            complainShopDetail = tracker.get_slot("complain_shop_detail")
            placeName = tracker.get_slot("place_name")
            imageLink = tracker.get_slot("image_link")
            userName = tracker.get_slot("user_name")
            phoneNumber = tracker.get_slot("phone_number")
            emailAddress = tracker.get_slot("email_address")

            owner_ref = {
            "ref": userId,
            "provider": provider_type,
            "displayName": userName
            }
            owner_ref_url = 'https://avc.bonmek.com/api/chat/retrieve_ref/'
            post_owner_ref = requests.post(url = owner_ref_url, data = owner_ref, headers=headers)

            print(post_owner_ref.json())

            owner_ref_data = post_owner_ref.json()

            create_ticket ={
                "title": "Complain", 
                "content": "Problem details: " +complainShopDetail+ "\n Image link: " +imageLink,
                "location": placeName, 
                "category": domainName, 
                "is_urgent": True, 
                "is_incognito": True, 
                "unit_lv1": 1, 
                "telephone": phoneNumber, 
                "email": emailAddress, 
                "owner_ref": owner_ref_data['ref'], 
                "submitted_date_time": report_date
            }
            create_ticket_url = 'https://avc.bonmek.com/api/chat/ticket/'
            ticket = requests.post(url = create_ticket_url, data = create_ticket, headers=headers)
            print(ticket.json())
        
        if latestAction == "utter_inform_in_progress":
            dispatcher.utter_message(text="Here is your ticket reference number: " + owner_ref_data['ref'] + "\n You can use it when you want to see your progress status in the topic 'Track progress status'.")
            # dispatcher.utter_message(text="Okay!!!")
        if latestAction == "utter_inform_in_progress_TH":
            dispatcher.utter_message(text="นี้คือหมายเลข ticket number สำหรับการรับเรื่องในครั้งนี้: " + owner_ref_data['ref'] + "\n คุณสามารถใช้หมายเลขนี้เพื่อติดตามสถานะการดำเนินการได้ที่หัวข้อ 'ติดตามสถานะการดำเนินการ'")
            # dispatcher.utter_message(text="โอเคค่ะ")

        return []
#API

# TrackProgress
# GET /api/chat/ticket/?ref=owner_ref_data['ref']
class TrackProgress(Action):

    def name(self):
        return "action_track_progress"  

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
            
        intent = tracker.latest_message['intent'].get('name')
        print("class TrackProgress: intent > ", intent)

        headers = {"Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxOTYwMDk5OTk3LCJpYXQiOjE2NDQ3Mzk5OTcsImp0aSI6IjEzYTUwYWFmNWFkNTQ2ODliODVhYzYxMmQ5YmQ2MjY0IiwiYWNjb3VudF90eXBlIjoiYXBpIiwidXNlcl9pZCI6ImNoYXRib3QifQ.L9YineS4C_G0bbGhb2cTnj77cxYqMWVF3OV-YwW3PAU"}
        user_ref = tracker.get_slot("user_info")
        track_progress_url = 'https://avc.bonmek.com/api/chat/ticket/?ref='+user_ref

        print("track_progress_url: ", track_progress_url)

        track_progress = requests.get(url = track_progress_url, headers=headers)
        print("check status track progress: ", track_progress)
        # print("\n \n Here is all track_progress.json():   \n\n", track_progress.json())
        
        historyData = track_progress.json()
        numHistory = historyData['count']
        print(numHistory)

        resultRC = historyData['results']

        if numHistory == 0:
            dispatcher.utter_message(text="This ticket reference number, " +user_ref+ ", is incorrect. Please, re-check it.")
        else:
            dispatcher.utter_message(text="You have filed "+str(numHistory)+" complaints/reports.")

            for i in range(numHistory):
                codeTitle = resultRC[i]['category_info']['title']
                print("check code title: ", codeTitle)

                content = resultRC[i]['content']
                print("check content: ", content)

                submitDateTime = resultRC[i]['submitted_date_time']
                print ("check submit date: ", submitDateTime)

                x = submitDateTime.split("T")
        
                date = x[0]
                time = x[1].split(".")
                time = time[0]

                print(date)
                print(time)

                status = resultRC[i]['status']
                print("check status: ", status)

                dispatcher.utter_message(text="Topic: " +codeTitle+ " \n " +content+ " \n Submitted date: " +date+ " " +time+ " \n Status: " + status)
        

            
        # if intent == 'provide_owner_user_info':
        # dispatcher.utter_message(text="Here is your progress status: \n" + track_progress.json())
        # if intent == 'track_progress_status_TH':
        #     dispatcher.utter_message(text="นี่คือสถานะการดำเนินการของคุณ: \n" + track_progress.json())

        return[] 
    

class CallAmbulance(Action):

    def name(self):
        return "action_call_ambulance"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        intent = tracker.latest_message['intent'].get('name')
        print("class CallAmbulance: intent > ", intent)

        if intent == 'tell_accident_problem_HARD':
            #send notification to a staff
            dispatcher.utter_message(text="I already sent a notification to our staff. Please tell me the following details for speed of execution.")
            return []
        if intent == 'tell_accident_problem_HARD_TH':
            dispatcher.utter_message(text="ตอนนี้อันดาได้ทำการส่งข้อมูลแจ้งเตือนไปยังเจ้าหน้าที่ เพื่อรับช่วงการดำเนินการต่อแล้วค่ะ กรุณาบอกข้อมูลรายละเอียดต่อไปนี้ เพื่อความรวดเร็วในการดำเนินการ")
            return []
        
class ActionUtterSlotValLostItem(Action):

    def name(self):
        return "action_utter_slots_values_lost_stuff"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        itemName = tracker.get_slot("item_name")
        placeName = tracker.get_slot("place_name")
        stuffDetail = tracker.get_slot("stuff_detail")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if itemName == None:
            itemName = " "

        if imageLink == "No photos":    
            utter = "Here is your report: \n - Your name: " +userName+ " \n - Topic: Lost item \n - Location: " +placeName+ " \n - Item details: " +itemName+ " > " +stuffDetail+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo of the item: " +imageLink
            
            dispatcher.utter_message(utter)
        else:    
            # utter = "Here is your report: \n"+
            #         " - Your name: " +userName+ " \n"+
            #         " - Topic: Lost item \n"+
            #         " - Location: " +placeName+ " \n"+
            #         " - Item details: " +itemName+ " > " +stuffDetail+ " \n"+
            #         " - Contact number: " +phoneNumber+ " \n"+
            #         " - E-mail address: " +emailAddress+ " \n"+
            #         " - Photo of the item: "
            utter = "Here is your report: \n - Your name: " +userName+ " \n - Topic: Lost item \n - Location: " +placeName+ " \n - Item details: " +itemName+ " > " +stuffDetail+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo of the item: "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValMissingPerson(Action):

    def name(self):
        return "action_utter_slots_values_missing_person"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        missingPersonName = tracker.get_slot("missing_person_name")
        imageLink = tracker.get_slot("image_link")
        lastTime = tracker.get_slot("lasttime")
        placeName = tracker.get_slot("place_name")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "No photos":    
            utter = "Here is your information: \n - Your name: " +userName+ " \n - Topic: Report missing person \n - Missing person's name: " +missingPersonName+ " \n - Last seen time: " +lastTime+ " \n - Last seen location: " +placeName+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo of this person: " +imageLink
            dispatcher.utter_message(utter)
        else:
            utter = "Here is your information: \n - Your name: " +userName+ " \n - Topic: Report missing person \n - Missing person's name: " +missingPersonName+ " \n - Last seen time: " +lastTime+ " \n - Last seen location: " +placeName+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo of this person: "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValPubutil(Action):

    def name(self):
        return "action_utter_slots_values_complain_public_utility"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        detailPubutil = tracker.get_slot("detail_of_public_utility")
        placeName = tracker.get_slot("place_name")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "No photos":    
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Topic: Complain about public utilities \n - Location: " +placeName+ " \n - Problem details: " +detailPubutil+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : " +imageLink
            dispatcher.utter_message(utter)
        else:
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Topic: Complain about public utilities \n - Location: " +placeName+ " \n - Problem details: " +detailPubutil+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValTroubMake(Action):

    def name(self):
        return "action_utter_slots_values_complain_trouble_maker"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        detailTroubmak = tracker.get_slot("detail_of_trouble_maker")
        placeName = tracker.get_slot("place_name")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "No photos":    
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Topic: Complain about trouble maker \n - Location: " +placeName+ " \n - Problem details: " +detailTroubmak+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : " +imageLink            
            dispatcher.utter_message(utter)
        else:
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Topic: Complain about trouble maker \n - Location: " +placeName+ " \n - Problem details: " +detailTroubmak+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []              

class ActionUtterSlotValRestaurant(Action):

    def name(self):
        return "action_utter_slots_values_complain_restaurant"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        shopName = tracker.get_slot("shop_name")
        placeName = tracker.get_slot("place_name")
        topicComplain = tracker.get_slot("topic_complain")
        complainShopDetail = tracker.get_slot("complain_shop_detail")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "No photos":    
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Restaurant name: " +shopName+ ", " +placeName+ " \n - Topic: Complain about a restuarant/cafe \n - Complain about: " +topicComplain+ " \n - Details: " +complainShopDetail+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : " +imageLink            
            dispatcher.utter_message(utter)
        else:
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Restaurant name: " +shopName+ ", " +placeName+ " \n - Topic: Complain about a restuarant/cafe \n - Complain about: " +topicComplain+ " \n - Details: " +complainShopDetail+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValAccom(Action):

    def name(self):
        return "action_utter_slots_values_complain_accommodation"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        shopName = tracker.get_slot("shop_name")
        placeName = tracker.get_slot("place_name")
        topicComplain = tracker.get_slot("topic_complain")
        complainShopDetail = tracker.get_slot("complain_shop_detail")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "No photos":    
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Accommodation name: " +shopName+ ", " +placeName+ " \n - Topic: Complain about an accommodation \n - Complain about: " +topicComplain+ " \n - Details: " +complainShopDetail+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : " +imageLink            
            dispatcher.utter_message(utter)
        else:
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Accommodation name: " +shopName+ ", " +placeName+ " \n - Topic: Complain about an accommodation \n - Complain about: " +topicComplain+ " \n - Details: " +complainShopDetail+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValTourAtt(Action):

    def name(self):
        return "action_utter_slots_values_complain_tourist_attraction"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        shopName = tracker.get_slot("shop_name")
        placeName = tracker.get_slot("place_name")
        topicComplain = tracker.get_slot("topic_complain")
        complainShopDetail = tracker.get_slot("complain_shop_detail")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "No photos":    
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Tourist attraction's name: " +shopName+ ", " +placeName+ " \n - Topic: Complain about a tourist attraction \n - Complain about: " +topicComplain+ " \n - Details: " +complainShopDetail+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : " +imageLink            
            dispatcher.utter_message(utter)
        else:
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Tourist attraction's name: " +shopName+ ", " +placeName+ " \n - Topic: Complain about a tourist attraction \n - Complain about: " +topicComplain+ " \n - Details: " +complainShopDetail+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValCarRent(Action):

    def name(self):
        return "action_utter_slots_values_complain_car_rental_shop"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        shopName = tracker.get_slot("shop_name")
        placeName = tracker.get_slot("place_name")
        topicComplain = tracker.get_slot("topic_complain")
        complainShopDetail = tracker.get_slot("complain_shop_detail")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "No photos":    
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Car rental shop's name: " +shopName+ ", " +placeName+ " \n - Topic: Complain about a car rental shop \n - Complain about: " +topicComplain+ " \n - Details: " +complainShopDetail+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : " +imageLink            
            dispatcher.utter_message(utter)
        else:
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Car rental shop's name: " +shopName+ ", " +placeName+ " \n - Topic: Complain about a car rental shop \n - Complain about: " +topicComplain+ " \n - Details: " +complainShopDetail+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValGuide(Action):

    def name(self):
        return "action_utter_slots_values_complain_guide"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        shopName = tracker.get_slot("shop_name")
        placeName = tracker.get_slot("place_name")
        topicComplain = tracker.get_slot("topic_complain")
        complainShopDetail = tracker.get_slot("complain_shop_detail")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "No photos":    
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Guide/travel agent name: " +shopName+ ", " +placeName+ " \n - Topic: Complain about a guide/travel agent \n - Complain about: " +topicComplain+ " \n - Details: " +complainShopDetail+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : " +imageLink            
            dispatcher.utter_message(utter)
        else:
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Guide/travel agent name: " +shopName+ ", " +placeName+ " \n - Topic: Complain about a guide/travel agent \n - Complain about: " +topicComplain+ " \n - Details: " +complainShopDetail+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValOtherService(Action):

    def name(self):
        return "action_utter_slots_values_complain_other_service_place"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        shopName = tracker.get_slot("shop_name")
        placeName = tracker.get_slot("place_name")
        topicComplain = tracker.get_slot("topic_complain")
        complainShopDetail = tracker.get_slot("complain_shop_detail")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "No photos":    
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Provider's name: " +shopName+ ", " +placeName+ " \n - Topic: Complain about other services \n - Complain about: " +topicComplain+ " \n - Details: " +complainShopDetail+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : " +imageLink            
            dispatcher.utter_message(utter)
        else:
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Provider's name: " +shopName+ ", " +placeName+ " \n - Topic: Complain about other services \n - Complain about: " +topicComplain+ " \n - Details: " +complainShopDetail+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValOtherProb(Action):

    def name(self):
        return "action_utter_slots_values_complain_other"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        shopName = tracker.get_slot("shop_name")
        placeName = tracker.get_slot("place_name")
        topicComplain = tracker.get_slot("topic_complain")
        complainShopDetail = tracker.get_slot("complain_shop_detail")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "No photos":    
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Topic: Complain other problems \n - Details: " +complainShopDetail+ " \n - Location: " +placeName+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : " +imageLink            
            dispatcher.utter_message(utter)
        else:
            utter = "Here is your complaint: \n - Your name: " +userName+ " \n - Topic: Complain other problems \n - Details: " +complainShopDetail+ " \n - Location: " +placeName+ " \n - Contact number: " +phoneNumber+ " \n - E-mail address: " +emailAddress+ " \n - Photo : "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []                                           

class SetNumInformation(Action):

    def name(self):
        return "action_set_num_information"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        latestAction = tracker.latest_action_name
        print("class SetNumInformation: latestAction is -> ", latestAction)

        #splitting the string
        words = latestAction.split("_")

        #slicing the list (negative index means index from the end)
        #-1 means the last element of the list
        slotInfoName = words[-2] + "_" + words[-1]
        print(slotInfoName)

        # phoneNum = 0
        # email = 1
        # imageLink = 2

        if slotInfoName == "phone_number":
            return [ SlotSet("info_val", 0)]
        elif slotInfoName == "email_address":
            return [ SlotSet("info_val", 1)]
        elif slotInfoName == "image_link":
            return [ SlotSet("info_val", 2)]
        
        return []

class CheckNone(Action):

    def name(self):
        return "action_check_what_none"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        intent = tracker.latest_message['intent'].get('name')
        print("class CheckNone: intent > ", intent)

        infoVal = tracker.get_slot("info_val")
        
        # phoneNum = 0
        # email = 1
        # imageLink = 2

        if infoVal == 0:
            if intent == "say_none" or intent == "deny":
                return [ SlotSet("phone_number", "No phone number")]
            elif intent == "say_none_TH" or intent == "deny_TH":
                return [ SlotSet("phone_number", "ไม่มีเบอร์โทรศัพท์")]

        elif infoVal == 1:
            if intent == "say_none" or intent == "deny":
                return [ SlotSet("email_address", "No email")]
            elif intent == "say_none_TH" or intent == "deny_TH":
                return [ SlotSet("email_address", "ไม่มีอีเมล์")]

        elif infoVal == 2:
            if intent == "say_none" or intent == "deny":
                return [SlotSet("image_link", "No photos")]
            elif intent == "say_none_TH" or intent == "deny_TH":
                return [SlotSet("image_link", "ไม่มีรูปถ่าย")]
        
        return []

class ActionResetAllSlots(Action):

    def name(self):
        return "action_reset_all_slots"

    def run(self, dispatcher, tracker, domain):
        return [AllSlotsReset()]


################## Chit-chat ###################
class ActionCorona(Action):

    def name(self) -> Text:
        return "action_corona_tracker"

    def run(self, dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        response = requests.get("https://covid19.ddc.moph.go.th/api/Cases/today-cases-by-provinces").json()
        
        intent = tracker.latest_message['intent'].get('name')
        print("class ActionCorona: intent > ", intent)

        if intent == 'corona_province':
            entities = tracker.latest_message['entities']
            
            print("Province ent ", entities)
            province = ""

            for e in entities:
                if e['entity'] == "province":
                    province = e['value']
                    
            print("province text", province)
            
            provinceEN = province

            if provinceEN == 'Phuket' or provinceEN == 'phuket':
                provinceEN = 'Phuket' 
                province = 'ภูเก็ต'	
            elif provinceEN == 'Krabi' or provinceEN == 'krabi':
                provinceEN = 'Krabi'
                province = 'กระบี่'
            elif provinceEN == 'Phang-nga' or provinceEN == 'phang-nga' or provinceEN == 'phangnga':
                provinceEN = 'Phang-nga'
                province = 'พังงา'
            

            for data in response:
                if data["province"] == province.title():
                    print("data['province'] == ", data["province"])
                    print(province.title())
                    message = "Date: "+ data['update_date']+" Newcase: " +str(data['new_case']) +" Total Case: "+ str(data['total_case'])
            
            print(message)
            dispatcher.utter_message(text="Covid situation report in " + provinceEN + " , Thailand")
            dispatcher.utter_message(message)

        return []

class ActionPlace_restaurant(Action):

    def name(self) -> Text:
        return "action_place_restaurant"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
            
        intent = tracker.latest_message['intent'].get('name')
        print(intent)

        if intent == 'ask_info_restaurant':
            dispatcher.utter_message(text="searching data from https://www.google.com/maps/search/restaurant")
            url = "https://www.google.com/maps/search/restaurant"
            # webbrowser.open(url, new=0, autoraise=True)
        if intent == 'ask_info_restaurant_TH':
            dispatcher.utter_message(text="ค้นหาข้อมูลได้จาก https://www.google.com/maps/search/restaurant")
            url = "https://www.google.com/maps/search/restaurant"
            # webbrowser.open(url, new=0, autoraise=True)
        return []

class ActionPlace_tourist(Action):

    def name(self) -> Text:
        return "action_place_tourist"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
            
        intent = tracker.latest_message['intent'].get('name')
        print(intent)

        if intent == 'ask_info_tourist':
            dispatcher.utter_message(text="searching data from https://www.google.com/maps/search/tourist")
            url = "https://www.google.com/maps/search/tourist"
            # webbrowser.open(url, new=0, autoraise=True)
        elif intent == 'ask_info_tourist_TH':
            dispatcher.utter_message(text="ค้นหาข้อมูลได้จาก https://www.google.com/maps/search/tourist")
            url = "https://www.google.com/maps/search/tourist"
            # webbrowser.open(url, new=0, autoraise=True)

        return []

class ActionPlace_hospital(Action):

    def name(self) -> Text:
        return "action_place_hospital"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        intent = tracker.latest_message['intent'].get('name')
        print(intent)

        if intent == 'tell_accident_problem_SOFT' or intent == 'ask_info_hospital':
            dispatcher.utter_message(text="searching data from https://www.google.com/maps/search/hospital")
            url = "https://www.google.com/maps/search/hospital"
            # webbrowser.open(url, new=0, autoraise=True)
        elif intent == 'tell_accident_problem_SOFT_TH' or intent == 'ask_info_hospital_TH':
            dispatcher.utter_message(text="ค้นหาข้อมูลโรงพยาบาลใกล้คุณได้จาก https://www.google.com/maps/search/hospital")
            url = "https://www.google.com/maps/search/hospital"
            # webbrowser.open(url, new=0, autoraise=True)

        return []

class ActionPlace_drugstore(Action):

    def name(self) -> Text:
        return "action_place_drugstore"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        intent = tracker.latest_message['intent'].get('name')
        print(intent)

        if intent == 'tell_accident_problem_SOFT' or intent == 'ask_info_drugstore':    
            dispatcher.utter_message(text="searching data from https://www.google.com/maps/search/drug+store")
            url = "https://www.google.com/maps/search/drug+store"
            # webbrowser.open(url, new=0, autoraise=True)
        elif intent == 'tell_accident_problem_SOFT_TH' or intent == 'ask_info_drugstore_TH':
            dispatcher.utter_message(text="ค้นหาข้อมูลร้านขายยาที่อยู่ใกล้คุณมากที่สุดได้จาก https://www.google.com/maps/search/drug+store")
            url = "https://www.google.com/maps/search/drug+store"
            # webbrowser.open(url, new=0, autoraise=True)

        return []

class ActionPlace_rentalcar(Action):

    def name(self) -> Text:
        return "action_place_rentalcar"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        intent = tracker.latest_message['intent'].get('name')
        print(intent)

        if intent == 'ask_info_rentalcar_shop':  
            dispatcher.utter_message(text="searching data from https://www.google.com/maps/search/rental+car")
            url = "https://www.google.com/maps/search/rental+car"
            # webbrowser.open(url, new=0, autoraise=True)
        elif intent == 'ask_info_rentalcar_shop_TH':
            dispatcher.utter_message(text="ค้นหาข้อมูลได้จาก https://www.google.com/maps/search/rental+car")
            url = "https://www.google.com/maps/search/rental+car"
            # webbrowser.open(url, new=0, autoraise=True)

        return []

class ActionPlace_hotel(Action):

    def name(self) -> Text:
        return "action_place_hotel"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
            
        intent = tracker.latest_message['intent'].get('name')
        print("class ActionPlace_hotel: intent > ", intent)

        if intent == 'ask_info_hotel':
            dispatcher.utter_message(text="You can search the information from https://www.google.com/maps/seacr/hotel")
            url = "https://www.google.com/maps/seacr/hotel"
            # webbrowser.open(url, new=0, autoraise=True)
        if intent == 'ask_info_hotel_TH':
            dispatcher.utter_message(text="ค้นหาข้อมูลได้จาก https://www.google.com/maps/seacr/hotel")
            url = "https://www.google.com/maps/seacr/hotel"
            # webbrowser.open(url, new=0, autoraise=True)

        return []

class ActionWeater(Action):

    def name(self) -> Text:
        return "action_weater_tracker"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
            
        intent = tracker.latest_message['intent'].get('name')
        print("class ActionWeater: intent > ", intent)

        if intent == 'ask_about_weather':
            dispatcher.utter_message(text="You can search the information from https://weather.com/weather/today/l/7.00,100.50?par=google")
        if intent == 'ask_about_weather_TH':
            dispatcher.utter_message(text="ค้นหาข้อมูลได้จาก https://weather.com/weather/today/l/7.00,100.50?par=google")
            url = "https://weather.com/weather/today/l/7.00,100.50?par=google"
            # webbrowser.open(url, new=0, autoraise=True)

        return []    

#---------------------TH-----------------------#
class CallAmbulanceTH(Action):

    def name(self):
        return "action_call_ambulance_TH"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        intent = tracker.latest_message['intent'].get('name')
        print("class CallAmbulanceTH: intent > ", intent)


        if intent == 'tell_accident_problem_HARD_TH':
            dispatcher.utter_message(text="ตอนนี้อันดาได้ทำการส่งข้อมูลแจ้งเตือนไปยังเจ้าหน้าที่ เพื่อรับช่วงการดำเนินการต่อแล้วค่ะ กรุณาบอกข้อมูลรายละเอียดต่อไปนี้ เพื่อความรวดเร็วในการดำเนินการ")
            return []

class ActionUtterSlotValLostItemTH(Action):

    def name(self):
        return "action_utter_slots_values_lost_stuff_TH"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        itemName = tracker.get_slot("item_name")
        placeName = tracker.get_slot("place_name")
        stuffDetail = tracker.get_slot("stuff_detail")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if itemName == None:
            itemName = " "

        if imageLink == "ไม่มีรูปถ่าย":    
            # utter = "สรุปข้อมูลที่แจ้งเข้ามา: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - หัวข้อ: แจ้งของหาย \n - สถานที่: " +placeName+ " \n - ลักษณะของสิ่งของ: " +itemName+ " > " +stuffDetail+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปภาพสิ่งของที่หายไป: " +imageLink
            utter = "สรุปข้อมูลที่แจ้งเข้ามา: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - หัวข้อ: แจ้งของหาย \n - สถานที่: " +placeName+ " \n - ลักษณะของสิ่งของ: " +stuffDetail+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปภาพสิ่งของที่หายไป: " +imageLink
            dispatcher.utter_message(utter)
        else:    
            utter = "สรุปข้อมูลที่แจ้งเข้ามา: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - หัวข้อ: แจ้งของหาย \n - สถานที่: " +placeName+ " \n - ลักษณะของสิ่งของ: " +stuffDetail+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปภาพสิ่งของที่หายไป: "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValMissingPersonTH(Action):

    def name(self):
        return "action_utter_slots_values_missing_person_TH"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        missingPersonName = tracker.get_slot("missing_person_name")
        imageLink = tracker.get_slot("image_link")
        lastTime = tracker.get_slot("lasttime")
        placeName = tracker.get_slot("place_name")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "ไม่มีรูปถ่าย":    
            utter = "สรุปข้อมูลที่แจ้งเรื่องเข้ามา: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - หัวข้อ: แจ้งคนหาย \n - ชื่อบุคคลที่หายไป: " +missingPersonName+ " \n - ช่วงเวลาที่พบเห็นครั้งล่าสุด: " +lastTime+ " \n - สถานที่ที่พบครั้งล่าสุด: " +placeName+ " \n - เบอร์โทรติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปภาพของบุคคลที่หายไป: " +imageLink
            dispatcher.utter_message(utter)
        else:
            utter = "สรุปข้อมูลที่แจ้งเรื่องเข้ามา: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - หัวข้อ: แจ้งคนหาย \n - ชื่อบุคคลที่หายไป: " +missingPersonName+ " \n - ช่วงเวลาที่พบเห็นครั้งล่าสุด: " +lastTime+ " \n - สถานที่ที่พบครั้งล่าสุด: " +placeName+ " \n - เบอร์โทรติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปภาพของบุคคลที่หายไป: "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValPubutilTH(Action):

    def name(self):
        return "action_utter_slots_values_complain_public_utility_TH"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        detailPubutil = tracker.get_slot("detail_of_public_utility")
        placeName = tracker.get_slot("place_name")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "ไม่มีรูปถ่าย":    
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้ร้องเรียน: " +userName+ " \n - หัวข้อ: ร้องเรียนสาธารณูปโภค \n - สถานที่: " +placeName+ " \n - รายละเอียดของปัญหา: " +detailPubutil+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: " +imageLink
            dispatcher.utter_message(utter)
        else:
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้ร้องเรียน: " +userName+ " \n - หัวข้อ: ร้องเรียนสาธารณูปโภค \n - สถานที่: " +placeName+ " \n - รายละเอียดของปัญหา: " +detailPubutil+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValTroubMakeTH(Action):

    def name(self):
        return "action_utter_slots_values_complain_trouble_maker_TH"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        detailTroubmak = tracker.get_slot("detail_of_trouble_maker")
        placeName = tracker.get_slot("place_name")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "ไม่มีรูปถ่าย":    
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้ร้องเรียน: " +userName+ " \n - หัวข้อ: ร้องเรียนผู้สร้างความเดือดร้อน \n - สถานที่: " +placeName+ " \n - รายละเอียดของปัญหา: " +detailTroubmak+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: " +imageLink            
            dispatcher.utter_message(utter)
        else:
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้ร้องเรียน: " +userName+ " \n - หัวข้อ: ร้องเรียนผู้สร้างความเดือดร้อน \n - สถานที่: " +placeName+ " \n - รายละเอียดของปัญหา: " +detailTroubmak+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []              

class ActionUtterSlotValRestaurantTH(Action):

    def name(self):
        return "action_utter_slots_values_complain_restaurant_TH"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        shopName = tracker.get_slot("shop_name")
        placeName = tracker.get_slot("place_name")
        topicComplain = tracker.get_slot("topic_complain")
        complainShopDetail = tracker.get_slot("complain_shop_detail")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "ไม่มีรูปถ่าย":    
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - ชื่อร้านค้า: " +shopName+ ", " +placeName+ " \n - หัวข้อ: ร้องเรียนร้านอาหาร \n - ร้องเรียนร้านค้าในด้าน: " +topicComplain+ " \n - รายละเอียดการร้องเรียน: " +complainShopDetail+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: " +imageLink            
            dispatcher.utter_message(utter)
        else:
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - ชื่อร้านค้า: " +shopName+ ", " +placeName+ " \n - หัวข้อ: ร้องเรียนร้านอาหาร \n - ร้องเรียนร้านค้าในด้าน: " +topicComplain+ " \n - รายละเอียดการร้องเรียน: " +complainShopDetail+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValAccomTH(Action):

    def name(self):
        return "action_utter_slots_values_complain_accommodation_TH"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        shopName = tracker.get_slot("shop_name")
        placeName = tracker.get_slot("place_name")
        topicComplain = tracker.get_slot("topic_complain")
        complainShopDetail = tracker.get_slot("complain_shop_detail")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "ไม่มีรูปถ่าย":    
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - ชื่อของที่พัก: " +shopName+ ", " +placeName+ " \n - หัวข้อ: ร้องเรียนเกี่ยวกับที่พัก \n - ร้องเรียนที่พักในด้าน: " +topicComplain+ " \n - รายละเอียดการร้องเรียน: " +complainShopDetail+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: " +imageLink            
            dispatcher.utter_message(utter)
        else:
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - ชื่อของที่พัก: " +shopName+ ", " +placeName+ " \n - หัวข้อ: ร้องเรียนเกี่ยวกับที่พัก \n - ร้องเรียนที่พักในด้าน: " +topicComplain+ " \n - รายละเอียดการร้องเรียน: " +complainShopDetail+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValTourAttTH(Action):

    def name(self):
        return "action_utter_slots_values_complain_tourist_attraction_TH"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        shopName = tracker.get_slot("shop_name")
        placeName = tracker.get_slot("place_name")
        topicComplain = tracker.get_slot("topic_complain")
        complainShopDetail = tracker.get_slot("complain_shop_detail")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "ไม่มีรูปถ่าย":       
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - ชื่อของสถานที่ท่องเที่ยว: " +shopName+ ", " +placeName+ " \n - หัวข้อ: ร้องเรียนสถานที่ท่องเที่ยว \n - ร้องเรียนสถานที่ท่องเที่ยวในด้าน: " +topicComplain+ " \n - รายละเอียดการร้องเรียน: " +complainShopDetail+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: " +imageLink            
            dispatcher.utter_message(utter)
        else:
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - ชื่อของสถานที่ท่องเที่ยว: " +shopName+ ", " +placeName+ " \n - หัวข้อ: ร้องเรียนสถานที่ท่องเที่ยว \n - ร้องเรียนสถานที่ท่องเที่ยวในด้าน: " +topicComplain+ " \n - รายละเอียดการร้องเรียน: " +complainShopDetail+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValCarRentTH(Action):

    def name(self):
        return "action_utter_slots_values_complain_car_rental_shop_TH"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        shopName = tracker.get_slot("shop_name")
        placeName = tracker.get_slot("place_name")
        topicComplain = tracker.get_slot("topic_complain")
        complainShopDetail = tracker.get_slot("complain_shop_detail")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "ไม่มีรูปถ่าย":    
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - ชื่อร้านเช่ารถ: " +shopName+ ", " +placeName+ " \n - หัวข้อ: ร้องเรียนร้านเช่ารถ \n - ร้องเรียนร้านเช่ารถในด้าน: " +topicComplain+ " \n - รายละเอียดการร้องเรียน: " +complainShopDetail+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: " +imageLink            
            dispatcher.utter_message(utter)
        else:
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - ชื่อร้านเช่ารถ: " +shopName+ ", " +placeName+ " \n - หัวข้อ: ร้องเรียนร้านเช่ารถ \n - ร้องเรียนร้านเช่ารถในด้าน: " +topicComplain+ " \n - รายละเอียดการร้องเรียน: " +complainShopDetail+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValGuideTH(Action):

    def name(self):
        return "action_utter_slots_values_complain_guide_TH"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        shopName = tracker.get_slot("shop_name")
        placeName = tracker.get_slot("place_name")
        topicComplain = tracker.get_slot("topic_complain")
        complainShopDetail = tracker.get_slot("complain_shop_detail")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "ไม่มีรูปถ่าย":    
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - ชื่อของไกด์หรือบริษัททัวร์: " +shopName+ ", " +placeName+ " \n - หัวข้อ: ร้องเรียนไกด์/บริษัททัวร์ \n - ร้องเรียนไกด์/บริษัททัวร์ในด้าน: " +topicComplain+ " \n - รายละเอียดการร้องเรียน: " +complainShopDetail+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: " +imageLink            
            dispatcher.utter_message(utter)
        else:
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - ชื่อของไกด์หรือบริษัททัวร์: " +shopName+ ", " +placeName+ " \n - หัวข้อ: ร้องเรียนไกด์/บริษัททัวร์ \n - ร้องเรียนไกด์/บริษัททัวร์ในด้าน: " +topicComplain+ " \n - รายละเอียดการร้องเรียน: " +complainShopDetail+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValOtherServiceTH(Action):

    def name(self):
        return "action_utter_slots_values_complain_other_service_place_TH"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        shopName = tracker.get_slot("shop_name")
        placeName = tracker.get_slot("place_name")
        topicComplain = tracker.get_slot("topic_complain")
        complainShopDetail = tracker.get_slot("complain_shop_detail")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "ไม่มีรูปถ่าย":    
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - ชื่อสถานที่: " +shopName+ ", " +placeName+ " \n - หัวข้อ: ร้องเรียนเกี่ยวกับสถานที่ให้บริการอื่น ๆ \n - ร้องเรียนในด้าน: " +topicComplain+ " \n - รายละเอียดการร้องเรียน: " +complainShopDetail+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: " +imageLink            
            dispatcher.utter_message(utter)
        else:
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - ชื่อสถานที่: " +shopName+ ", " +placeName+ " \n - หัวข้อ: ร้องเรียนเกี่ยวกับสถานที่ให้บริการอื่น ๆ \n - ร้องเรียนในด้าน: " +topicComplain+ " \n - รายละเอียดการร้องเรียน: " +complainShopDetail+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []

class ActionUtterSlotValOtherProbTH(Action):

    def name(self):
        return "action_utter_slots_values_complain_other_TH"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        shopName = tracker.get_slot("shop_name")
        placeName = tracker.get_slot("place_name")
        topicComplain = tracker.get_slot("topic_complain")
        complainShopDetail = tracker.get_slot("complain_shop_detail")
        imageLink = tracker.get_slot("image_link")
        userName = tracker.get_slot("user_name")
        phoneNumber = tracker.get_slot("phone_number")
        emailAddress = tracker.get_slot("email_address")

        if imageLink == "ไม่มีรูปถ่าย":    
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - หัวข้อ: ร้องเรียนเรื่องอื่น ๆ \n - รายละเอียด: " +complainShopDetail+ " \n - สถานที่ที่เกี่ยวข้อง: " +placeName+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: " +imageLink            
            dispatcher.utter_message(utter)
        else:
            utter = "สรุปข้อมูลการร้องเรียน: \n - ชื่อ-สกุลผู้แจ้ง: " +userName+ " \n - หัวข้อ: ร้องเรียนเรื่องอื่น ๆ \n - รายละเอียด: " +complainShopDetail+ " \n - สถานที่ที่เกี่ยวข้อง: " +placeName+ " \n - เบอร์ติดต่อ: " +phoneNumber+ " \n - อีเมล์: " +emailAddress+ " \n - รูปถ่าย: "
            dispatcher.utter_message(utter)
            dispatcher.utter_message(image=imageLink)

        return []


################## Chit-chat ###################
class ActionCoronaTH(Action):

    def name(self) -> Text:
        return "action_corona_tracker_TH"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
            
        response = requests.get("https://covid19.ddc.moph.go.th/api/Cases/today-cases-by-provinces").json()
        
        intent = tracker.latest_message['intent'].get('name')
        print("class ActionCoronaTH: intent > ", intent)

        if intent == 'corona_province_TH':
            entities = tracker.latest_message['entities']
            print("Last Message Now ", entities)
            province = ""

            for e in entities:
                    if e['entity'] == "province":
                        province = e['value']

            for data in response:
                if data["province"] == province.title():
                    print(data)
                    message = "วันที่: "+ data['update_date']+" ผู้ติดเชื้อรายใหม่: " +str(data['new_case']) +" ยอดรวมสุทธิ: "+ str(data['total_case'])
            dispatcher.utter_message(text="รายงานสถานการณ์ COVID-19 ในจังหวัด" + province)
            dispatcher.utter_message(message)

        return []
