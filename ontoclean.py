import typing
from typing import Any, Optional, Text, Dict, List, Type

from rasa.nlu.components import Component
from rasa.nlu.config import RasaNLUModelConfig
from rasa.shared.nlu.training_data.training_data import TrainingData
from rasa.shared.nlu.training_data.message import Message

if typing.TYPE_CHECKING:
    from rasa.nlu.model import Metadata


from rasa.shared.nlu.constants import ENTITIES, TEXT, INTENT
from rasa.nlu.tokenizers.tokenizer import Token

from rdflib import Graph, Namespace, Literal, URIRef
from rdflib.namespace import RDF

class WICClassifier(Component):
    """A new component"""

    # Which components are required by this component.
    # Listed components should appear before the component itself in the pipeline.
    @classmethod
    def required_components(cls) -> List[Type[Component]]:
        """Specify which components need to be present in the pipeline."""

        return []

    # Defines the default configuration parameters of a component
    # these values can be overwritten in the pipeline configuration
    # of the model. The component should choose sensible defaults
    # and should be able to create reasonable results with the defaults.
    defaults = {}

    # Defines what language(s) this component can handle.
    # This attribute is designed for instance method: `can_handle_language`.
    # Default value is None which means it can handle all languages.
    # This is an important feature for backwards compatibility of components.
    supported_language_list = None

    # Defines what language(s) this component can NOT handle.
    # This attribute is designed for instance method: `can_handle_language`.
    # Default value is None which means it can handle all languages.
    # This is an important feature for backwards compatibility of components.
    not_supported_language_list = None

    

    def openDB(self):
        self.graph = Graph('BerkeleyDB', identifier='wic6')
        self.graph.open('triplestore/wic6', create=True)

    def __init__(self, component_config: Optional[Dict[Text, Any]] = None) -> None:
        super().__init__(component_config)
        self.openDB()
        self.is_init = False

    def train(
        self,
        training_data: TrainingData,
        config: Optional[RasaNLUModelConfig] = None,
        **kwargs: Any,
    ) -> None:
        """Train this component.

        This is the components chance to train itself provided
        with the training data. The component can rely on
        any context attribute to be present, that gets created
        by a call to :meth:`components.Component.pipeline_init`
        of ANY component and
        on any context attributes created by a call to
        :meth:`components.Component.train`
        of components previous to this one."""
        self.openDB()
        WIC = Namespace("http://streamsouth.tech/wic#")

        #training_data = training_data.training_examples 
        
    
        for example in training_data.entity_examples:
            intent = example.get(INTENT).replace(" ","_")
            sentence = example.get(TEXT).replace(" ","_")

            intent = URIRef("http://streamsouth.tech/wic#"+intent)
            sentence = URIRef("http://streamsouth.tech/wic#"+sentence)
          
            hasExamplePhrase = URIRef("http://streamsouth.tech/wic#hasExamplePhrase")
            isExamplePhraseOf = URIRef("http://streamsouth.tech/wic#hasExamplePhrase")
            
            isEntityOf = URIRef("http://streamsouth.tech/wic#isEntityOf")
            hasEntity = URIRef("http://streamsouth.tech/wic#hasEntity")

            isRelatedToIntent = URIRef("http://streamsouth.tech/wic#isRelatedToIntent")
            

            self.graph.add((intent, RDF.type, WIC.Intent))
            self.graph.add((sentence, RDF.type, WIC.Example))
            self.graph.add((intent, hasExamplePhrase, sentence))
            self.graph.add((sentence, isExamplePhraseOf, intent))
            
            for entity in example.get(ENTITIES, []):
                ent = entity.get("value").replace(" ","_")
                ent = URIRef("http://streamsouth.tech/wic#"+ent)
                self.graph.add((ent, RDF.type, WIC.Entity))

                self.graph.add((ent, isEntityOf, sentence))
                self.graph.add((sentence, hasEntity, ent))
                
                self.graph.add((ent, isRelatedToIntent, intent))
                self.graph.add((intent, hasEntity, ent))
        

        #print(self.graph.serialize())
       # self.graph.serialize(format="pretty-xml", destination="test4.rdf")
        #self.graph.close()
        self.is_init = False

    def process(self, message: Message, **kwargs: Any) -> None:
        """Process an incoming message.

        This is the components chance to process an incoming
        message. The component can rely on
        any context attribute to be present, that gets created
        by a call to :meth:`components.Component.pipeline_init`
        of ANY component and
        on any context attributes created by a call to
        :meth:`components.Component.process`
        of components previous to this one."""
        #self.openDB()
        #print("Incoming Message "+message.get(INTENT))
        #print(message.get(TEXT)+"\n")
        if self.is_init:
            self.openDB()
            self.is_init = True

        for entity in message.get(ENTITIES, []):
            query = """
                PREFIX wic:<http://streamsouth.tech/wic#>
                SELECT ?intent WHERE {
            """
            query += "<http://streamsouth.tech/wic#"+entity.get("value").replace(" ", "_")+">  <http://streamsouth.tech/wic#isRelatedToIntent> ?intent . }"      
            qres = self.graph.query(query)
            for row in qres:
                #print(f"{row.intent}")
                #print(row.intent.split('#')[1])
                message.set("intent", {"name":row.intent.split('#')[1], "confidence":1.00}, add_to_output=True)

        #self.graph.close()
        #self.is_init = False
        


    def persist(self, file_name: Text, model_dir: Text) -> Optional[Dict[Text, Any]]:
        """Persist this component to disk for future loading."""
        #self.graph.close()
        pass

    @classmethod
    def load(
        cls,
        meta: Dict[Text, Any],
        model_dir: Text,
        model_metadata: Optional["Metadata"] = None,
        cached_component: Optional["Component"] = None,
        **kwargs: Any,
    ) -> "Component":
        """Load this component from file."""
        #self.openDB()
        if cached_component:
            return cached_component
        else:
            return cls(meta)
